<script type="text/javascript">
var costum = <?php echo json_encode(Yii::app()->session['costum']) ?>;
    //if(typeof costum.appRendering != "undefined")
    themeParams=<?php echo json_encode(Yii::app()->session['paramsConfig']) ?>;
    costum.init = function(where){
        if(costum.logo){
            $(".topLogoAnim").remove();
            $(".logo-menutop, .logoLoginRegister").attr({'src':costum.logo});
        }
        if(typeof costum.filters != "undefined"){
            if(typeof costum.filters.scopes != "undefined"){
                if(typeof costum.filters.scopes.cities != "undefined")
                    setOpenBreadCrum({'cities': costum.filters.scopes.cities });
            }
            if(typeof costum.filters.sourceKey != "undefined")
                searchObject.sourceKey=costum.request.sourceKey;
        }
        if(typeof costum.css != "undefined"){
            initCssCustom(costum.css);
        }
        if(typeof costum.headerParams != "undefined"){
            $.each(costum.headerParams, function(e, v){
                if(typeof headerParams[e] != "undefined"){
                    $.each(v, function(name, value){
                        headerParams[e][name]=value;
                    });
                }
            });
        }
        if(typeof costum.htmlConstruct != "undefined" 
            && typeof costum.htmlConstruct.directory != "undefined"
            && typeof costum.htmlConstruct.directory.viewMode != "undefined")
            directoryViewMode=costum.htmlConstruct.directory.viewMode;

    };
    costum.initMenu = function(where){
    }
   
    if(typeof costum.css != "undefined"){
        if(typeof costum.css.loader !="undefined"){ 
            color1=(typeof costum.css.loader.ring1 != "undefined" && costum.css.loader.ring1.color != "undefined") ? costum.css.loader.ring1.color : "#ff9205";
            color2=(typeof costum.css.loader.ring2 != "undefined" && costum.css.loader.ring2.color != "undefined") ? costum.css.loader.ring2.color : "#3dd4ed";
            themeObj.blockUi = {
                processingMsg :'<div class="lds-css ng-scope">'+
                        '<div style="width:100%;height:100%" class="lds-dual-ring">'+
                            '<img src="'+costum.logo+'" class="loadingPageImg" height=80>'+
                            '<div style="border-color: transparent '+color2+' transparent '+color2+';"></div>'+
                            '<div style="border-color: transparent '+color1+' transparent '+color1+';"></div>'+
                        '</div>'+
                    '</div>', 
                errorMsg : '<img src="'+costum.logo+'" class="logo-menutop" height=80>'+
                  '<i class="fa fa-times"></i><br>'+
                   '<span class="col-md-12 text-center font-blackoutM text-left">'+
                    '<span class="letter letter-red font-blackoutT" style="font-size:40px;">404</span>'+
                   '</span>'+

                  '<h4 style="font-weight:300" class=" text-dark padding-10">'+
                    'Oups ! Une erreur s\'est produite'+
                  '</h4>'+
                  '<span style="font-weight:300" class=" text-dark">'+
                    'Vous allez être redirigé vers la page d\'accueil'+
                  '</span>'
            };
        }
    }
    function getStyleCustom(cssObject){
        cssAdd="";
        if(typeof cssObject.background != "undefined")
            cssAdd+="background-color:"+cssObject.background+" !important;";
        if(typeof cssObject.border != "undefined")
            cssAdd+="border:"+cssObject.border+" !important;";
        if(typeof cssObject.borderBottom != "undefined")
            cssAdd+="border-bottom:"+cssObject.borderBottom+" !important;";
        if(typeof cssObject.box != "undefined")
            cssAdd+="box-shadow:"+cssObject.box+" !important;";
        if(typeof cssObject.boxShadow != "undefined")
            cssAdd+="box-shadow:"+cssObject.boxShadow+" !important;";
        if(typeof cssObject.fontSize != "undefined")
            cssAdd+="font-size:"+cssObject.fontSize+"px !important;";
        if(typeof cssObject.color != "undefined")
            cssAdd+="color:"+cssObject.color+" !important;";
        if(typeof cssObject.paddingTop != "undefined")
            cssAdd+="padding-top:"+cssObject.paddingTop+" !important;";
        if(typeof cssObject.height != "undefined")
            cssAdd+="height:"+cssObject.height+"px !important;";
        if(typeof cssObject.width != "undefined")
            cssAdd+="width:"+cssObject.width+"px !important;";
        if(typeof cssObject.top != "undefined")
            cssAdd+="top:"+cssObject.top+"px !important;";
        if(typeof cssObject.bottom != "undefined")
            cssAdd+="bottom:"+cssObject.bottom+"px !important;";
        if(typeof cssObject.right != "undefined")
            cssAdd+="right:"+cssObject.right+"px !important;";
        if(typeof cssObject.left != "undefined")
            cssAdd+="left:"+cssObject.left+"px !important;";
        if(typeof cssObject.borderWidth != "undefined")
            cssAdd+="border-width:"+cssObject.borderWidth+"px !important;";
        if(typeof cssObject.borderColor != "undefined")
            cssAdd+="border-color:"+cssObject.borderColor+" !important;";
        if(typeof cssObject.borderRadius != "undefined")
            cssAdd+="border-radius:"+cssObject.borderRadius+"px !important;";
        if(typeof cssObject.lineHeight != "undefined")
            cssAdd+="line-height:"+cssObject.lineHeight+"px !important;";
        if(typeof cssObject.padding != "undefined")
            cssAdd+="padding:"+cssObject.padding+" !important;";
        if(typeof cssObject.display != "undefined")
            cssAdd+="display:"+cssObject.display+" !important;";
        
        return cssAdd;
    }
    function initCssCustom(style){
        str="<style type='text/css'>";
        if(typeof style.font != "undefined"){
            str+="@font-face {font-family: 'customFont';src: url('"+assetPath+"/"+style.font.url+"')}";
            str+="body, h1, h2, h3, h4, h5, h6, button, input, select, textarea, a, p, span{font-family: 'customFont' !important;}";
        }
        if(typeof style.menuTop != "undefined"){
            str+="#mainNav{"+getStyleCustom(style.menuTop)+"}";
            if(typeof style.menuTop.logo != "undefined")
                str+="#mainNav .logo-menutop{"+getStyleCustom(style.menuTop.logo)+"}";
            if(typeof style.menuTop.button != "undefined")
                str+="#mainNav .menu-btn-top{"+getStyleCustom(style.menuTop.button)+"}";
            if(typeof style.menuTop.badge != "undefined")
                str+=".btn-menu-notif .notifications-count, .btn-menu-chat .chatNotifs, .btn-dashboard-dda .coopNotifs{"+getStyleCustom(style.menuTop.badge)+"}";
            if(typeof style.menuTop.scopeBtn != "undefined"){
                str+=".menu-btn-scope-filter{"+getStyleCustom(style.menuTop.scopeBtn)+"}";
                if(typeof style.menuTop.scopeBtn.hover != "undefined")
                    str+=".menu-btn-scope-filter:hover, .menu-btn-scope-filter.active{"+getStyleCustom(style.menuTop.scopeBtn.hover)+"}";
            }
            if(typeof style.menuTop.filterBtn != "undefined")
                str+=".btn-show-filters{"+getStyleCustom(style.menuTop.filterBtn)+"}";
            if(typeof style.menuTop.connectBtn != "undefined")
                str+="#mainNav .btn-menu-connect{"+getStyleCustom(style.menuTop.connectBtn)+"}";
        }
        if(typeof style.menuApp != "undefined"){
            str+="#territorial-menu{"+getStyleCustom(style.menuApp)+"}";
            if(typeof style.menuApp.button != "undefined")
                str+="#territorial-menu .btn-menu-to-app{"+getStyleCustom(style.menuApp.button)+"}";
            if(typeof style.menuApp.button.hover != "undefined")
                str+="#territorial-menu .btn-menu-to-app:hover, #territorial-menu .btn-menu-to-app:active,#territorial-menu .btn-menu-to-app:focus, #territorial-menu .btn-menu-to-app.active{"+getStyleCustom(style.menuApp.button.hover)+"}";
        }
        if(typeof style.progress != "undefined"){
            if(style.progress.bar != "undefined")
                str+=".progressTop::-webkit-progress-bar { "+getStyleCustom(style.progress.bar)+" }";
            if(style.progress.value != "undefined")
                str+=".progressTop::-webkit-progress-value{"+getStyleCustom(style.progress.value)+"}.progressTop::-moz-progress-bar{"+getStyleCustom(style.progress.value)+"}";
        }
        if(typeof style.loader != "undefined"){
            if(typeof style.loader.background != "undefined")
                str+=".blockUI.blockMsg.blockPage{background-color:"+style.loader.background+" !important}";
            if(typeof style.loader.ring1 != "undefined")
                str+=".lds-dual-ring div{"+getStyleCustom(style.loader.ring1)+"}";
            if(typeof style.loader.ring2 != "undefined")
                str+=".lds-dual-ring div:nth-child(2){"+getStyleCustom(style.loader.ring2)+"}";
        }
        if(typeof style.button != "undefined"){
            if(typeof style.button.footer != "undefined"){
                if(typeof style.button.footer.add != "undefined")
                    str+="#show-bottom-add{"+getStyleCustom(style.button.footer.add)+"}";
                if(typeof style.button.footer.toolbarAdds != "undefined")
                    str+=".toolbar-bottom-adds{"+getStyleCustom(style.button.footer.toolbarAdds)+"}";
                if(typeof style.button.footer.donate != "undefined")
                    str+="#donation-btn{"+getStyleCustom(style.button.footer.donate)+"}";
            }
        }
        if(typeof style.color != "undefined"){
            colorCSS=["purple", "orange", "red", "black", "green", "green-k", "yellow", "yellow-k", "vine", "brown"];
            $.each(colorCSS, function(e,v){
                if(typeof style.color[v] != "undefined"){
                    str+=".text-"+v+"{color:"+style.color[v]+" !important;}.bg-"+v+"{background-color:"+style.color[v]+" !important;}";
                }
            });
        }
        str+="@media (max-width: 767px){"+
                "#mainNav{margin-top:0px !important;}"+
                "#mainNav .menu-btn-top{font-size:22px !important}}"+
                "#mainNav .logo-menutop{height:40px}"+
            "}";
        str+="</style>"; 

        $("head").append(str);
    }
</script>

