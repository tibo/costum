
<div class="pageContent">

<style type="text/css">
.row_title {
  margin-top:10px;
}

#bg-homepage {
  width:100%;
}

.numberCircle {
  font-size: 50px;
  font-weight: bold;
  margin-right: 15px;
}

p.step {
  font-size:30px;
  font-weight:bold;
  display : flex;
  align-items : center;
}

ul {
    list-style: none;
    margin-left: 0;
    padding-left: 1.2em;
    text-indent: -1.2em;
}

li:before {
    content: "►";
    display: block;
    float: left;
    width: 1.2em;
    color: #00B1E4;
}

li {
  font-size:1.5em;
  margin-bottom:10px;
}

</style>

<div class="row row_title">
		<p class="text-center">
			<img src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mocica/title_mocica3.png' alt="MOCICA" class="mocica" style="width:450px;margin-left:40px;"/>
			<img src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mocica/title_logo.png' alt="MOCICA" class="logo" style="width:170px;"/>
		</p>
		<h1 class="phrase text-center">Unis et Libres</h1>
	</div>

  <div class="container">
  <p>
  Bienvenue sur la plate-forme du Mocica. Ce mouvement propose, en 3 étapes, le passage à un
modèle de société sans argent, géré par une Organisation Démocratique Globale. Sans
dirigeant, l'ODG permet à chacun de prendre part aux décisions qui le concernent, du niveau
le plus local (son quartier), jusqu'au niveau le plus global (le monde).
  </p>

  <p class="step" style="color:#F44336;"><span class='numberCircle'><!-- &#x2776; -->&#x2780;</span> Créer des assemblées de quartiers</p>
  <p>Permettre aux membres du Mocica de se retrouver et former des assemblées de quartiers, 
  base de notre gouvernance autogérée ODG (Organisation Démocratique Globale)</p>

  <p class="step" style="color:#448AFF;"><span class='numberCircle'><!-- &#x2777; -->&#x2781;</span> Diffuser le projet Mocica</p>
  <p>Chaque assemblée a pour objectif de sympathiser et de promouvoir
  le projet Mocica afin de multiplier les assemblées à travers le monde.</p>
  
  <p class="step" style="color:#4CAF50;"><span class='numberCircle'><!-- &#x2778; -->&#x2782;</span> Débattre de notre organisation future</p>

  <p class="step" style="color:#9A1B7E;"><span class='numberCircle'><!-- &#x2779; -->&#x2783;</span> Concrétiser les 3 étapes du projet Mocica :</p>
<p>
<span style='font-weight:bold;'>Rassemblement – Transition – Organisation</span> <br/> Tous les détails du projet Mocica <a href='https://mocica.org'>ici</a>.
</p>
<p>
<br/>
Vous trouverez sur cette plateforme de nombreux outils :
<ul>
<li>Créer des assemblées</li>
<li>Espace de discussion / débat icônes</li>
<li>Une cartographie</li>
<li>Faire des annonces</li>
<li>Sondage</li>
<li>Un agenda commun</li>
<li>Un espace d’entraide</li>
<li>Télécharger des documents</li>
</p>
  </div>
    
</div>

<script type="text/javascript">
  jQuery(document).ready(function() {
    setTitle("Projet MOCICA Unis et Libre");
  });
</script>

