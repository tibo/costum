<?php

//assets from ph base repo
$cssAnsScriptFilesTheme = array(
	// SHOWDOWN
	'/plugins/showdown/showdown.min.js',
	//MARKDOWN
	'/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

//gettting asstes from parent module repo
$cssAnsScriptFilesModule = array( 
	'/js/dataHelpers.js',
	'/css/md.css',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl() );

//if( $this->layout != "//layouts/empty"){
//	$layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
//	$this->renderPartial($layoutPath.'header',array("page"=>"welcome","layoutPath"=>$layoutPath));
//}
?>
<style type="text/css">
	table, th, td {
  border: 1px solid black;
}
table{
	width: 100%;
}
table, th, td {
  padding: 15px;
}
#doc h1 {
	margin-left: 0px;
    padding: 0px;
    margin-bottom: 30px;
    background-color: white;
    float: left;
    color: #4285f4;
    border: 0px;
    border-bottom: 1px solid #4285f4 !important;
    width: 100%;
}
#doc h3 {
	margin-left: 0px;
    padding: 0px;
    margin-bottom: 20px;
    background-color: white;
    float: left;
    color: #e6344d;
    font-size: 20px;
    border: 0px;
    margin-top: 10px;
    width: 100%;
    text-align: left;
}
#mainNav{
	display:none;
}
.menu-doc-header a{
	font-size: 16px;
	color: white;
	line-height: 50px;
}
.redirect-custom.border-right{
	border-right: 2px solid white; 
}
.redirect-custom{
	padding: 10px 50px;
}
#examples, #offers{
	display: none;
}
.redirect-custom.active{
	background-color: white;
    color: #e6344d;
    border-radius: 5px;
    margin-left: 2px;
    margin-right: 0px;
}
header{
	background-color: #e6344d;
	color: white;
	margin-bottom: 30px;
}
</style>
<header class="col-xs-12">
<h1 style="text-align: center;padding:10px; font-size:24px; text-transform: inherit;">
	<i class="fa fa-mask"></i> <?php echo CHtml::encode( (isset($this->module->pageTitle))?$this->module->pageTitle:""); ?>
</h1>
<nav class="col-xs-12 menu-doc-header">
	<a href="javascript:;" class="redirect-custom border-right active" data-id="doc">
		Documentation
	</a>
	<a href="javascript:;" class="redirect-custom border-right" data-id="examples">
		Examples
	</a>
	<a href="javascript:;" class="redirect-custom" data-id="offers">
		Offres
	</a>
</nav>
</header>
<div id="doc" class="col-xs-12 content-section-docs"></div>
<div id="examples" class="col-xs-12 content-section-docs">
	<?php $costums=PHDB::find("costum");
		$html="";
		foreach($costums as $key=> $v){
			$el = Slug::getElementBySlug($v["slug"], ["shortDescription", "profilImageUrl", "profilThumbImageUrl", "profilMediumImageUrl", "name", "tags", "description"]);
			$el=$el["el"];
			$img=$this->module->getParentAssetsUrl()."/images/wave.jpg";
			if (@$v["metaImg"]) 
		      	$img = Yii::app()->getModule( $this->module->id )->getAssetsUrl().$v["metaImg"];
		    else if(!empty($el["profilMediumImageUrl"]))
		      	$img = Yii::app()->createUrl($el["profilImageUrl"]);
			$html.="<div class='col-xs-12 col-sm-4 shadow2 margin-bottom-10' style='min-height:300px;height:400px; overflow-y:hidden;'>".
						"<div class='col-xs-12'>".
							"<img src='".$img."' class='img-responsive'/>".
						"</div>".
						"<div class='col-xs-12'>".
							"<a href='".Yii::app()->createUrl("/costum/co/index/id/".$v["slug"])."' class='col-xs-12 elipsis' target='_blank'>".
								"<h3>".$el["name"]."</h3>".
							"</a>".
							"<span class='col-xs-12'>".@$el["shortDescription"]."</span>".
								"<div class='col-xs-12 letter-red'>";
							if(!empty($el["tags"])){
								foreach($el["tags"] as $v){
									$html.="#".$v;
								}
							}
						$html.="</div></div>".
					"</div>";
		}
		echo $html;
	?>

</div>
<div id="offers" class="col-xs-12 content-section-docs">
	<span>Tibo your first integration: Todo List</span>
	<p>
		[] expliquer l'offre<br/>
		[] proposer un devis dynamique<br/>
			[] Qui je suis<br/> 
			[] Besoin<br/>
			[] Estimationb calculé de manière dynamique<br/> 
	</p>
</div>

<script type="text/javascript">

$(document).ready(function() { 
	getAjax('', baseUrl+'/<?php  echo $this->module->id;?>/default/doc',
		function(data){ 
			showdown.setOption('tables', true);
    		converter = new showdown.Converter();
    		text      = data;
    		descHtml      = converter.makeHtml(text); 
			$('#doc').html(descHtml);
		},"html");
	$(".redirect-custom").click(function(){
		$(".redirect-custom").removeClass("active");
		$(this).addClass("active");
		contentDomSection=$(this).data("id");
		$(".content-section-docs").hide(100);
		$("#"+contentDomSection).show(500);
	});
});

</script>
