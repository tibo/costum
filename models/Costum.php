<?php

class Costum {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	
	//used in initJs.php for the modules definition
	public static function getConfig($context=null){
		return array(
			"collection"    => self::COLLECTION,
            "controller"   => self::CONTROLLER,
            "module"   => self::MODULE,
            "categories" => CO2::getModuleContextList(self::MODULE, "categories", $context),
       		"lbhp" => true
		);
	}

	/**
	 * get a Poi By Id
	 * @param String $id : is the mongoId of the poi
	 * @return poi
	 */
	public static function getById($id) { 
	  	$classified = PHDB::findOneById( self::COLLECTION ,$id );
	  	//$classified["parent"] = Element::getElementByTypeAndId()
	  	$classified["typeClassified"]=@$classified["type"];
	  	$classified["type"]=self::COLLECTION;
	  	$where=array("id"=>@$id, "type"=>self::COLLECTION, "doctype"=>"image");
	  	$classified["images"] = Document::getListDocumentsWhere($where, "image");//(@$id, self::COLLECTION);
	  	return $classified;
	}

	/**
	* Function called by @self::filterThemeInCustom in order to treat params.json of communecter app considering like the config
	* This loop permit to genericly update value, add values or delete value in the tree
	* return the communecter entry filtered by costum expectations
	**/
	public static function checkCOstumList($check, $filter){
        $newObj=array();
        foreach($check as $key => $v){
            if(!empty($v)){ 
                $newObj[$key]=(@$filter[$key]) ? $filter[$key] : $v;
                $checkArray=true;
                $arrayInCustom=["label","subdomainName","placeholderMainSearch", "icon","height","imgPath","useFilter","slug","formCreate", "initType"];
                foreach($arrayInCustom as $entry){
                    if(@$v[$entry]){
                        $newObj[$key][$entry]=$v[$entry];
                        $checkArray=false;
                    }
                }
                if(is_array($v) && $checkArray)
                    $newObj[$key]=self::checkCOstumList($v, $newObj[$key]);
            }
        }
        return $newObj;
    }

    /*
    * @filterThemeInCustom permits to clean, update or add entry in paramsConfig of communecter thanks to costumParams 
    *	Receive Yii::app()->session["paramsConfig"] in order to modify it if Yii::app()->session["costum"] is existed
    *	
    */
    public static function filterThemeInCustom($params){
        $menuApp=array("#annonces", "#search", "#agenda", "#live", "#dda");
        //filter menu app custom 
        // Html construction of communecter
        if(@Yii::app()->session["costum"]["htmlConstruct"]){
            $constructParams=Yii::app()->session["costum"]["htmlConstruct"];
            //var_dump($params);exit;
            // Check about app (#search, #live, #etc) if should costumized
            if(@$constructParams["app"]){
                $params["numberOfApp"]=count($constructParams["app"]);
                $menuPages=self::checkCOstumList($constructParams["app"], $params["pages"]);
                foreach($params["pages"] as $hash => $v){
                    if(!in_array($hash,$menuApp))
                        $menuPages[$hash]=$v;
                }
                $params["pages"]=$menuPages;
            }
            // Check about header construction
            // - Entry banner to adda tpl path to a custom banner, by default not isset
            // - Entry menuTop will config btn present in menu top (navLeft & navRight) 
            if(@$constructParams["header"]){
                if(@$constructParams["header"]["banner"])
                    $params["header"]["banner"]=@$constructParams["header"]["banner"];
                if(@$constructParams["header"]["css"])
                    $params["header"]["css"]=@$constructParams["header"]["css"];
                if(@$constructParams["header"]["menuTop"]){
                    if(@$constructParams["header"]["menuTop"]["navLeft"])
                        $params["header"]["menuTop"]["navLeft"]=self::checkCOstumList($constructParams["header"]["menuTop"]["navLeft"],$params["header"]["menuTop"]["navLeft"]);
                    if(@$constructParams["header"]["menuTop"]["navRight"])
                        $params["header"]["menuTop"]["navRight"]=self::checkCOstumList($constructParams["header"]["menuTop"]["navRight"],$params["header"]["menuTop"]["navRight"]);
                }
            }
            // Check about admin Panel navigation (cosstumized space for costum administration expectations)
            if(@$constructParams["adminPanel"])
                $params["adminPanel"]=self::checkCOstumList($constructParams["adminPanel"],$params["adminPanel"]);
            // Check about element page organization and content
            // - #initView is a param to give first view loaded when arrived in a page (ex : newstream, detail, agenda or another)
            // - #menuLeft of element => TODO PLUG ON checkCOstumList
            // - #menuTop of element => TODO finish customization & add it in params.json
            if(@$constructParams["element"]){
                if(@$constructParams["element"]["initView"])
                    $params["element"]["initView"]=@$constructParams["element"]["initView"];
                if(@$constructParams["element"]["menuLeft"]){
                    foreach($params["element"]["menuLeft"] as $key => $v){
                        if(@$constructParams["element"]["menuLeft"][$key]){
                            if(@$constructParams["element"]["menuLeft"][$key]["label"])
                                $params["element"]["menuLeft"][$key]["label"]=$constructParams["element"]["menuLeft"][$key]["label"];
                            if(@$constructParams["element"]["menuLeft"][$key]["icon"])
                                $params["element"]["menuLeft"][$key]["icon"]=$constructParams["element"]["menuLeft"][$key]["icon"];
                        }else{
                            unset($params["element"]["menuLeft"][$key]);
                        }
                    }
                }
               
            }
            // #appRendering permit to get horizontal and vertical organization of menu 
            //	 - horizontal = first version of co2 with menu of apps in horizontal 
            //	 - vertical = current version of co2 with menu of apps on the left 
            if(@$constructParams["appRendering"])
                $params["appRendering"]=$constructParams["appRendering"];
            if(@$constructParams["directory"])
                $params["directory"]=$constructParams["directory"];

            // # redirect permits to application in case of undefined view or error or automatic redirection to fall on costum redirect
            // To be setting for logged user && unlogged user
            if(@$constructParams["redirect"])
                $params["pages"]["#app.index"]["redirect"]=$constructParams["redirect"];
            if(@$constructParams["footer"])
                $params["footer"]=$constructParams["footer"];
        }
        if(@Yii::app()->session["costum"]["add"])
            $params["add"]=Yii::app()->session["costum"]["add"];
        return $params;
    }

}
?>