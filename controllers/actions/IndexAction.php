<?php
class IndexAction extends CAction
{
    public function run($id=null)
    { 	
    	$this->getController()->layout = "//layouts/mainSearch";
		//$params = CO2::getThemeParams();
    	//Yii::app()->session['paramsConfig']=$params;
		if(strlen($id) == 24 && ctype_xdigit($id)  )
	        $c = PHDB::findOne( "costum" , array("_id"=> new MongoId($id)));
	    else 
	   		$c = PHDB::findOne( "costum" , array("slug"=> $id));
	   	if(!empty($c)){ 
        	$el = Slug::getElementBySlug($c["slug"], ["shortDescription", "profilImageUrl", "profilThumbImageUrl", "profilMediumImageUrl", "name", "tags", "description"]);
        	if(!empty($el["el"])){
        		$element=array(
        		    "contextType" => $el["type"],
	                "contextId" => $el["id"],
	                "title"=> $el["el"]["name"],
	                "description"=> @$el["el"]["shortDescription"],
                   	"tags"=> @$el["el"]["tags"],
                   	"assetsUrl"=> Yii::app()->getModule($this->getController()->module->id)->getAssetsUrl(true),
                   	"url"=> "/costum/co/index/id/".$c["slug"] );

        		if( @$el["el"]["profilImageUrl"] ) 
        			$c["logo"] = Yii::app()->createUrl($el["el"]["profilImageUrl"]);
        		 if(@$c["logoMin"]){
		            $c["logoMin"] = Yii::app()->getModule( $this->getController()->module->id )->getAssetsUrl().$c["logoMin"];
		        }
        		$c["admins"]= Element::getCommunityByTypeAndId($el["type"], $el["id"], Person::COLLECTION,"isAdmin");
	        	if(@Yii::app()->session["userIsAdmin"] && !@$c["admins"][Yii::app()->session["userId"]])
	            	$c["admins"][Yii::app()->session["userId"]]=array("type"=>Person::COLLECTION, "isAdmin"=>true);
	    		if(@$c["admins"][Yii::app()->session["userId"]])
	    			Yii::app()->session['isCostumAdmin']=true;
        		$c = array_merge( $c , $element );

	        	/* metadata */
		      	$shortDesc =  @$element["shortDescription"] ? $element["shortDescription"] : "";
		      	if($shortDesc=="")
		      		$shortDesc = @$element["description"] ? $element["description"] : "";

		      	$this->getController()->module->description = @$shortDesc;
		      	$this->getController()->module->pageTitle = @$element["name"];
		      	$this->getController()->module->keywords = (@$element["tags"]) ? implode(",", @$element["tags"]) : "";
		      	if (@$c["favicon"]) 
		      		$this->getController()->module->favicon =  Yii::app()->getModule( $this->getController()->module->id )->getAssetsUrl().$c["favicon"];
		      	if (@$c["metaImg"]) 
		      		$this->getController()->module->image = Yii::app()->getModule( $this->getController()->module->id )->getAssetsUrl().$c["metaImg"];
		      	else if(@$c["logo"])
		      		$this->getController()->module->image = $c["logo"];
		      	/* metadata */
        	}
    		$c["request"]["sourceKey"]=[$c["slug"]];    
		    Yii::app()->session['costum'] = $c;
		    //if(!@Yii::app()->session['paramsConfig'] || empty(Yii::app()->session['paramsConfig'])) 
    		    Yii::app()->session['paramsConfig'] = CO2::getThemeParams(); 
    		//var_dump(Yii::app()->session['paramsConfig']);
	    	Yii::app()->session["paramsConfig"]=Costum::filterThemeInCustom(Yii::app()->session["paramsConfig"]);
    	}//else
    		//Yii::app()->session["costum"] = null;
	  	$this->getController()->render("index");
    }
}